void AnalysisGammaCsI()
{
   TCanvas *c1 = new TCanvas();

   TFile *input = new TFile("/home/chung/Geant4/cebr3_simulation/CeBr3_Simulation/build/OutputTest.root", "read");

   TTree *tree = (TTree*)input->Get("ScoringGammaParticles");

   double edepSecondary;
   tree->SetBranchAddress("edepSecondary", &edepSecondary);

   int entries = tree->GetEntries();

   cout << entries << endl;

   TH1F *hist = new TH1F("hist","Photons yield Energy in Detector", 10000, -0.10, 5.0);

  for(int i = 0; i < entries; i++)
   {
     tree->GetEntry(i);
     //cout << edepSecondary << endl;
      hist->Fill(edepSecondary);
    }

   hist->Draw();
   hist->GetXaxis()->SetTitle("E_{gamma} (MeV)");
   hist->GetYaxis()->SetTitle("Entries");
         
 //  input->Close();
}
