#include "construction.hh"

MyDetectorConstruction::MyDetectorConstruction()
{
// setting up the messenger for UI command  and Detector construction
   fMessenger  = new G4GenericMessenger(this, "/detector/","Detector Construction");


   fMessenger->DeclareProperty("rmax_OuterTube", rmax_OuterTube, "Radius of outer tube in mm");
  
   fMessenger->DeclareProperty("hz_OuterTube", hz_OuterTube, "Length of detector in mm");


  rmax_OuterTube = 19.;
  hz_OuterTube = 19.;
  DefineMaterials();

 }

MyDetectorConstruction::~MyDetectorConstruction()
{}

void MyDetectorConstruction::DefineMaterials()
{
    // defining material from Nist librabry : Vacuum
   G4NistManager *nist = G4NistManager::Instance();
   worldMat = nist->FindOrBuildMaterial("G4_Galactic");
   
    // defining CeriumBromide(CeBr3) material 
  CeBr3 = new G4Material("CeBr3", 5.2*g/cm3, 2);
  CeBr3->AddElement(nist->FindOrBuildElement("Ce"), 1);
  CeBr3->AddElement(nist->FindOrBuildElement("Br"), 3);
 
 }

G4VPhysicalVolume *MyDetectorConstruction::Construct()
{
  // defining the world volume: solid made of Air, logical to placement
   G4double xWorld = 75.0*mm;
   G4double yWorld = 75.0*mm;
   G4double zWorld = 100.0*mm;

  solidWorld = new G4Box("solidWorld", xWorld, yWorld, zWorld);

  logicWorld = new G4LogicalVolume(solidWorld, worldMat,"logicWorld");

  physWorld = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicWorld, "physWorld", 0, false, 0, true);


  // Outer Tube
  G4double rmin_OuterT = 0.;
  G4double rmax_OuterT = rmax_OuterTube*mm; /// manipulated radius
  G4double  hz_OuterT = hz_OuterTube*mm;
  G4double phimin_OuterT = 0.;
  G4double dphi_OuterT = 360.*deg;

  // Outer tube volume
  outerTubeSolid = new G4Tubs("OuterTube", rmin_OuterT, rmax_OuterT, hz_OuterT, phimin_OuterT, dphi_OuterT);

  logicOuterTube
    = new G4LogicalVolume(outerTubeSolid, CeBr3, "OuterTube");

    // setting the OuterTube to be the scoring volume for Photon yield
  fScoringVolume = logicOuterTube; 

  physOuterTube  = new G4PVPlacement(0, 
                    G4ThreeVector(0.,0.,0.),       //at (0,0,0)
                    logicOuterTube,              //its logical volume
                    "OuterTube",                //its name
                    logicWorld,               //its mother  volume
                    false,                 //no boolean operation
                    0,                     //copy number
                    true);        //overlaps checking


 
  // Visualization attributes
  auto visAttributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0));
  visAttributes->SetVisibility(false);
  logicWorld->SetVisAttributes(visAttributes);

  visAttributes = new G4VisAttributes(G4Colour(0.90,0.9,0.90));
  logicOuterTube->SetVisAttributes(visAttributes);


   return physWorld;
}

// Sensitive Detector for scoring development
void MyDetectorConstruction::ConstructSDandField()
{
    MySensitiveDetector *sensDet = new MySensitiveDetector("SensitiveDetector");

//    logicDetector->SetSensitiveDetector(sensDet);
}


