#include "event.hh"

MyEventAction::MyEventAction(MyRunAction*)
{
   fEdep = 0.;
   fEdepSecondary = 0.;
   fEdepSecondaryEP = 0.;
}

MyEventAction::~MyEventAction()
{}

void MyEventAction::BeginOfEventAction(const G4Event*)
{
  fEdep = 0.;
  fEdepSecondary = 0.;
  fEdepSecondaryEP = 0.;

}

void MyEventAction::EndOfEventAction(const G4Event*)
{   
  //  Energy deposition filling
  G4cout << "Energy deposition: " << fEdep << G4endl;
  G4cout << "Secondary Particle Energy deposition: " << fEdepSecondary << G4endl;
  G4AnalysisManager *man = G4AnalysisManager::Instance();
  
  man->FillNtupleDColumn(1,0,fEdep);
  man->AddNtupleRow(1);

  man->FillNtupleDColumn(2,0,fEdepSecondary);
  man->AddNtupleRow(2);
  
  man->FillNtupleDColumn(4,0,fEdepSecondaryEP);
  man->AddNtupleRow(4);

}

