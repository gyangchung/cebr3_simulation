#include "generator.hh"

MyPrimaryGenerator::MyPrimaryGenerator()
{ 
  fParticleGun = new G4ParticleGun(1);
  fParticleGun2 = new G4ParticleGun(1);
  
  // Particle to generate from table: electron
   G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
   G4String particleName = "neutron";
   //G4String particleName2 = "neutron";  // to test
   G4String particleName2 = "gamma";
   G4ParticleDefinition *particle = particleTable->FindParticle(particleName);
   G4ParticleDefinition *particle2 = particleTable->FindParticle(particleName2);

  // randomized direction
    G4double dtheta = 15.*deg;
    G4double dphi = 360.*deg;
    G4double theta = G4UniformRand()*dtheta -7.5*deg;
    G4double phi = G4UniformRand()*dphi;

  // Source size: a multiple of 4.5
    G4double Source_size_multiple = 1.0 ;
  
  // Particle Source Position and dimension of source
    G4double x0 =  Source_size_multiple*2.0*(G4UniformRand()-0.5)*mm;
    G4double y0 =  Source_size_multiple*2.0*(G4UniformRand()-0.5)*mm;
    G4double z0 = -49.0*mm + Source_size_multiple*2.0*(G4UniformRand()-0.5)*mm;

//  setting particle properties: position, momentum and energy
    G4ThreeVector pos(x0, y0, z0); //for Square solid 
   // G4ThreeVector mom(0.0, 0.0, -1.0); 
    G4ThreeVector mom(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta));

  fParticleGun->SetParticlePosition(pos);
  fParticleGun->SetParticleMomentumDirection(mom);
  fParticleGun->SetParticleEnergy(G4RandGauss::shoot(2.0*MeV,0.01));
  fParticleGun->SetParticleDefinition(particle);
  
  // randomized direction
    G4double dtheta2 = 15.*deg;
    G4double dphi2 = 360.*deg;
    G4double theta2 = G4UniformRand()*dtheta2 -7.5*deg;
    G4double phi2 = G4UniformRand()*dphi2;
    // Position and momentum of gamma generator
    G4ThreeVector pos2(x0, y0, z0+0.01*mm);
    G4ThreeVector mom2(sin(theta2)*cos(phi2), sin(theta2)*sin(phi2), cos(theta2));

  fParticleGun2->SetParticlePosition(pos2);
  fParticleGun2->SetParticleMomentumDirection(mom2);
  fParticleGun2->SetParticleEnergy(G4RandGauss::shoot(4.439*MeV,0.01));
  fParticleGun2->SetParticleDefinition(particle2);

}

MyPrimaryGenerator::~MyPrimaryGenerator()
{
  delete fParticleGun;
  delete fParticleGun2;
}


void MyPrimaryGenerator::GeneratePrimaries(G4Event *anEvent)
{ 
   fParticleGun->GeneratePrimaryVertex(anEvent);
   fParticleGun2->GeneratePrimaryVertex(anEvent);

}
