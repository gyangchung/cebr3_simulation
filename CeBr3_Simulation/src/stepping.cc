#include "stepping.hh"

MySteppingAction::MySteppingAction(MyEventAction* eventAction)
{
  fEventAction = eventAction;
}

MySteppingAction::~MySteppingAction()
{}

void MySteppingAction::UserSteppingAction(const G4Step *step)
{ 
  // to get acces to the  volume that the particle is
  G4LogicalVolume *volume  = step->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetLogicalVolume();
 
  // to be certain of the detector scoring volume
  const MyDetectorConstruction *detectorConstruction = static_cast<const MyDetectorConstruction*>(G4RunManager::GetRunManager()->GetUserDetectorConstruction());

  // desired volume that we want to get the energy in it
  G4LogicalVolume *fScoringVolume = detectorConstruction->GetScoringVolume();
 
  // comparing the volume that the particle is and the volume we want to score
  if(volume != fScoringVolume)
      return; 
   // the if conndition does not allowing scoring in other volume

  G4double edep = step->GetTotalEnergyDeposit();
  fEventAction->AddEdep(edep);

   G4Track* track = step->GetTrack(); 
/* 
 // Here, one get the energy deposited of any secondary particle my earlier errors
  if ( track->GetParentID() > 0 )//This is a secondary Particle
    {
      //track->GetDefinition() returns the particle definition 
       // G4double edepSecondary = step->GetTotalEnergyDeposit();
       G4double edepSecondary = step->GetTrack()->GetKineticEnergy(); //poststep  energy
       fEventAction->AddEdepSecondary(edepSecondary); 
                // Fill Gamma energy when created
        G4AnalysisManager *man = G4AnalysisManager::Instance();  
        man->FillNtupleDColumn(3,0,edepSecondary);
        man->AddNtupleRow(3);

     }
*/      
// Here we get the energy of gamma deposited
   if (track->GetDynamicParticle()->GetDefinition()==G4Gamma::Gamma())//This is a secondary Particle 
    {   // Get total energy deposited by yield Gamma in the detector
        G4double edepSecondary = step->GetTotalEnergyDeposit();
        fEventAction->AddEdepSecondary(edepSecondary);

        // Fill Gamma energy when created
        G4double eGammaSecondary = step->GetPreStepPoint()->GetTotalEnergy();
        G4AnalysisManager *man = G4AnalysisManager::Instance();  
        man->FillNtupleDColumn(3,0,eGammaSecondary);
        man->AddNtupleRow(3);
    }

// Here we get the energy of Secondary Electron Positron created and deposited
   if (track->GetParentID() > 0 && track->GetDynamicParticle()->GetDefinition() !=G4Gamma::Gamma())//This is a secondary Particle 
    {   // Get total energy deposited by yield Gamma in the detector
        G4double edepSecondaryElectronPositron = step->GetTotalEnergyDeposit();
        fEventAction->AddEdepSecondaryEP(edepSecondaryElectronPositron);

        // Fill electron and positron energy when created
        G4double SecondaryElectronPositron = step->GetPreStepPoint()->GetTotalEnergy();
        G4AnalysisManager *man = G4AnalysisManager::Instance();  
        man->FillNtupleDColumn(5,0,SecondaryElectronPositron);
        man->AddNtupleRow(5);
    }


}
