#include "detector.hh"


MySensitiveDetector::MySensitiveDetector(G4String name) :
G4VSensitiveDetector(name)
{}

MySensitiveDetector::~MySensitiveDetector()
{}

G4bool MySensitiveDetector::ProcessHits(G4Step *aStep, G4TouchableHistory *ROhist)
{
   // Get the track of the photon
    G4Track *track  = aStep->GetTrack();
    track->SetTrackStatus(fStopAndKill);

   // Get the prestep and post steps of the photon in a track
    G4StepPoint  *preStepPoint = aStep->GetPreStepPoint();
    G4StepPoint  *postStepPoint = aStep->GetPostStepPoint();

    // Get the position of the photon and print it on the screen
    G4ThreeVector posPhoton = preStepPoint->GetPosition();

//   G4cout << "Photon position" << posPhoton << G4endl;

   // Get the copy number of the detector sensor cell that the photon touch
   const G4VTouchable *touchable  = aStep->GetPreStepPoint()->GetTouchable();
   G4int copyNo = touchable->GetCopyNumber();

   // G4cout << "Copy number: " << copyNo << G4endl;

   // Get the position of the detector sensor cell that the photon touch
   G4VPhysicalVolume *physVol = touchable->GetVolume();
   G4ThreeVector posDetector = physVol->GetTranslation();

   G4cout  << "Detector position: " << posDetector << G4endl;
  
   // collecting information from detector
   G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

   // filling collected information from detector
   G4AnalysisManager *man = G4AnalysisManager::Instance();

   man->FillNtupleIColumn(0,evt);
   man->FillNtupleDColumn(1,posDetector[0]);
   man->FillNtupleDColumn(2,posDetector[1]);
   man->FillNtupleDColumn(3,posDetector[2]);
   man->AddNtupleRow(0);

}
