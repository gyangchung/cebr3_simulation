#include "action.hh"


MyActionInitialization::MyActionInitialization()
{}

MyActionInitialization::~MyActionInitialization()
{}

void MyActionInitialization::BuildForMaster() const
{
    //  Initialization of runaction for master thread
   MyRunAction *runAction = new MyRunAction();
   SetUserAction(runAction);
}


void MyActionInitialization::Build() const
{
  // Initialization of generator
   MyPrimaryGenerator *generator = new MyPrimaryGenerator();
   SetUserAction(generator);

 //  Initialization of runaction
   MyRunAction *runAction = new MyRunAction();
   SetUserAction(runAction);

    //  Initialization of eventaction
   MyEventAction *eventAction = new MyEventAction(runAction);
   SetUserAction(eventAction);

 //  Initialization of steppingaction
   MySteppingAction *steppingAction = new MySteppingAction(eventAction);
   SetUserAction(steppingAction);
}
