#include "physics.hh"
#include "G4DecayPhysics.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"
#include "G4HadronPhysicsShielding.hh"

MyPhysicsList::MyPhysicsList()
{
     // Registering the relevant/needed physics for the current setup
     // RegisterPhysics(new G4EmStandardPhysics());  // high energy model
        RegisterPhysics(new G4EmStandardPhysics_option4());  // low energy model
     // RegisterPhysics(new G4EmLivermorePhysics());
      // RegisterPhysics(new G4EmPenelopePhysics());
      RegisterPhysics(new G4OpticalPhysics());
      RegisterPhysics(new G4HadronElasticPhysicsHP());
      RegisterPhysics(new G4RadioactiveDecayPhysics());
      RegisterPhysics(new G4DecayPhysics());
      RegisterPhysics(new G4HadronPhysicsQGSP_BIC_HP());
      RegisterPhysics(new G4HadronPhysicsShielding());
}

MyPhysicsList::~MyPhysicsList()
{}
