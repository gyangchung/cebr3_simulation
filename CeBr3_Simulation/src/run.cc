#include "run.hh"


MyRunAction::MyRunAction()
{
  // filling file format
  G4AnalysisManager *man = G4AnalysisManager::Instance();
  man->CreateNtuple("Hits","Hits");
  man->CreateNtupleIColumn("fEvent");
  man->CreateNtupleDColumn("fX");
  man->CreateNtupleDColumn("fY");
  man->CreateNtupleDColumn("fZ");
  man->FinishNtuple(0);

  // scoring for energy deposited
  man->CreateNtuple("Scoring","Scoring");
  man->CreateNtupleDColumn("fEdep");
  man->FinishNtuple(1);

  // scoring for Secondary particles energy deposited
  man->CreateNtuple("ScoringSecondaryParticles","ScoringSecondaryParticles");
  man->CreateNtupleDColumn("fEdepSecondary");
  man->FinishNtuple(2);

  // scoring for Secondary gamma particles energy
  man->CreateNtuple("ScoringGammaParticles","ScoringGammaParticles");
  man->CreateNtupleDColumn("edepSecondary");
  man->FinishNtuple(3);

  // scoring for Secondary electrons and positrons energy deposited
  man->CreateNtuple("ScoringSecondaryElectronsPositrons","ScoringSecondaryElectronsPositrons");
  man->CreateNtupleDColumn("fEdepSecondaryEP");
  man->FinishNtuple(4);

  // scoring for Secondary electrons and positrons energy
  man->CreateNtuple("SecondaryElectronPositron","SecondaryElectronPositron");
  man->CreateNtupleDColumn("SecondaryElectronPositron");
  man->FinishNtuple(5);

}

MyRunAction::~MyRunAction()
{}

void MyRunAction::BeginOfRunAction(const G4Run* run)
{
  G4AnalysisManager *man = G4AnalysisManager::Instance();
  // Obtaining Run ID 
  G4int  runID = run->GetRunID();

  std::stringstream strRunID;
  strRunID << runID;  
  // Open a root file to fill based on run_number
   //man->OpenFile("OutputOutputA7070"+strRunID.str()+".root");
   man->OpenFile("OutputTest.root");

}

void MyRunAction::EndOfRunAction(const G4Run*)
{
  G4AnalysisManager *man = G4AnalysisManager::Instance();

  // Write to th openned root file
  man->Write();
  man->CloseFile();

}
