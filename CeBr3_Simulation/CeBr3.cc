#include <iostream>

#include "construction.hh"
#include "physics.hh"
#include "generator.hh"
#include "action.hh"

#include "G4RunManager.hh"
#include "G4UIExecutive.hh"
#include "G4UImanager.hh"
#include "G4VisManager.hh"
#include "G4VisExecutive.hh"

#include "G4ios.hh"

int main(int argc, char** argv)
{
   // setting user interaction access 
   G4UIExecutive *ui = 0;

     // including multithreaded mode
  #ifdef G4MULTITREADED
       G4MTRunManager *runManager = new G4MTRunManager();
   #else
       G4RunManager *runManager = new G4RunManager();
   #endif

   // Initializing my detector construction
   runManager->SetUserInitialization(new MyDetectorConstruction());

   // Initializing my relevant Physics
   runManager->SetUserInitialization(new MyPhysicsList());

   // Initializing my Action for paricles 
   runManager->SetUserInitialization(new MyActionInitialization());

  // Initializing run manager
//  runManager->Initialize(); //should come first; taken to run macro

 // setting condtion to execute with or without visualization
  if(argc == 1)
  {
   ui = new G4UIExecutive(argc, argv);

  }

  // setting visualization access
   G4VisManager *visManager = new G4VisExecutive();
   visManager->Initialize();

   G4UImanager *UImanager = G4UImanager::GetUIpointer();

  if(ui)
  {
   // Applying some commands for visualization in vis.mac
   UImanager->ApplyCommand("/control/execute vis.mac");
   ui->SessionStart();
   }
   else
  {
     G4String command = "/control/execute ";
     G4String fileName = argv[1];
     UImanager->ApplyCommand(command+fileName);
   }


   return 0;
}
