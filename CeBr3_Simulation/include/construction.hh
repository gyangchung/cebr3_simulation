
#ifndef CONSTRUCTION_HH
#define CONSTRUCTION_HH

#include "G4VUserDetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4GenericMessenger.hh"

#include "G4VisAttributes.hh" 
#include "G4Colour.hh"
#include "detector.hh"

class MyDetectorConstruction : public G4VUserDetectorConstruction
{
public:
    MyDetectorConstruction();
    ~MyDetectorConstruction();

   // function to select volume for energy deposition
   G4LogicalVolume *GetScoringVolume() const {return fScoringVolume; }

   virtual G4VPhysicalVolume *Construct();

 private:
    virtual void ConstructSDandField();

    G4int nCols, nRows; //DetMaterial;    
    G4double rmax_OuterTube, hz_OuterTube;
 

   // Initialization for easy acces and change
    G4Box *solidWorld;
    G4VSolid *outerTubeSolid;
    G4LogicalVolume *logicWorld, *logicOuterTube;
    G4VPhysicalVolume *physWorld,*physOuterTube;
   
  G4Material *CeBr3, *worldMat;
 
    G4LogicalVolume *fScoringVolume;

    G4GenericMessenger *fMessenger;

   void DefineMaterials();
};

#endif
